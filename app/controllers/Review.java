package controllers;

import models.Problem;
import models.ReviewCode;
import models.User;
import play.data.validation.Required;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.With;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@With(Secure.class)
public class Review extends Controller {

    @Transactional(readOnly = true)
    public static void index() {
        User user = User.find("byLogin", Security.connected()).first();
        List<ReviewCode> codeReviews = null;
        if (user.role.id == 1) { //programmer
            codeReviews = ReviewCode.findByUser(user);
        }
        else { //reviewer
            codeReviews = ReviewCode.findAll();
        }
        render(user, codeReviews);
    }

    public static void addCode() {
        User user = User.find("byLogin", Security.connected()).first();
        render(user);
    }

    public static void saveCode(Long id, @Required String notice, @Required String code) {
        User user = User.findById(id);
        ReviewCode.addReviewCode(user, notice, code);
        index();
    }

    @Transactional(readOnly = true)
    public static void code(Long id) {
        ReviewCode code = ReviewCode.findById(id);
        User user = User.find("byLogin", Security.connected()).first();
        render(code, user);
    }

    public static void saveProblem(Long id, @Required String problem) {
        Problem.addProblem(id, problem);
        code(id);
    }

    public static void update(Long id, @Required String notice, @Required String code) {
        ReviewCode.update(id, notice, code);
        code(id);
    }

    public static void soluteProblem(Long id, Boolean value) {
        Problem p = Problem.findById(id);
        p.soluted = value;
        p.save();
    }

    public static void accept(Long id, Long uid) {
        ReviewCode rc = ReviewCode.findById(id);
        User rev = User.findById(uid);
        Timestamp time = new Timestamp(Calendar.getInstance().getTime().getTime());
        rc.acceptDate = time;
        rc.accepted = true;
        rc.reviewer = rev;
        Set<Problem> problemSet = rc.problems;
        for(Problem p : problemSet) {
            p.soluted = true;
            p.solutionDate = time;
            p.save();
        }
        rc.save();
        code(id);
    }

}