package controllers;


import models.User;

public class Security extends Secure.Security {

    static boolean authenticate(String username, String password) {
        User user = User.find("from User as u where u.login = ? and u.password = ?",
                username, password).first();
        return user != null;
    }

    static boolean check(String profile) {
        User user = User.find("byLogin", profile).first();
        if (user.role.id == 2) {
            return true;
        }
        return false;
    }
}
