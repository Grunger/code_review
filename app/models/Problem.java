package models;


import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
@Table(name = "problems")
public class Problem extends GenericModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "problems_id_seq")
    @SequenceGenerator(name = "problems_id_seq", sequenceName = "problems_id_seq", allocationSize = 1)
    public Long id;

    @Column(name = "problem")
    public String problem;

    @Column(name = "soluted")
    public Boolean soluted;

    @ManyToOne
    @JoinColumn(name = "review_code_id")
    public ReviewCode reviewCode;

    @Column(name = "add_date")
    public Timestamp addDate;

    @Column(name = "solution_date")
    public Timestamp solutionDate;

    public Problem() {

    }

    public static void addProblem(Long reviewCode, String problem) {
        ReviewCode rc = ReviewCode.findById(reviewCode);
        Problem p = new Problem();
        p.problem = problem;
        p.reviewCode = rc;
        p.soluted = false;
        p.addDate = new Timestamp(Calendar.getInstance().getTime().getTime());
        p.save();
    }
}
