package models;


import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "review_code")
public class ReviewCode extends GenericModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "review_code_id_seq")
    @SequenceGenerator(name = "review_code_id_seq", sequenceName = "review_code_id_seq", allocationSize = 1)
    public Long id;

    @Column(name = "code")
    public String code;

    @Column(name = "accepted")
    public Boolean accepted;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    public User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "reviewer_id")
    public User reviewer;

    @Column(name = "add_date")
    public Timestamp addDate;

    @Column(name = "accept_date")
    public Timestamp acceptDate;

    @Column(name = "notice")
    public String notice;

    @Column(name = "update_date")
    public Timestamp updateDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reviewCode")
    public Set<Problem> problems;

    public ReviewCode() {

    }

    public static void addReviewCode(User u, String n, String c) {
        ReviewCode reviewCode = new ReviewCode();
        reviewCode.user = u;
        reviewCode.notice = n;
        reviewCode.code = c;
        Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        reviewCode.addDate = timestamp;
        reviewCode.updateDate = timestamp;
        reviewCode.save();
    }

    public static List<ReviewCode> findByUser(User u) {
        List<ReviewCode> r = ReviewCode.find("byUser", u).fetch();
        return r;
    }

    public static void update(Long id, String notice, String code) {
        ReviewCode rc = ReviewCode.findById(id);
        rc.updateDate = new Timestamp(Calendar.getInstance().getTime().getTime());
        rc.notice = notice;
        rc.code = code;
        rc.save();
    }

}
