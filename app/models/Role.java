package models;

import play.db.jpa.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="roles")
public class Role extends GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "roles_id_seq")
    @SequenceGenerator(name = "roles_id_seq", sequenceName = "roles_id_seq", allocationSize = 1)
    @Column(name="id")
    public Long id;

    @Column(name="name", length = 32)
    public String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
    public Set<User> users;

    public Role() {

    }

}
