package models;

import play.db.jpa.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "users_id_seq")
    @SequenceGenerator(name="users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    public Long id;

    @Column(name="login", length = 32)
    public String login;

    @Column(name="password", length = 32)
    public String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    public Role role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public Set<ReviewCode> users;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reviewer")
    public Set<ReviewCode> reviewers;

    public User() {

    }

}
