﻿create table roles(
id serial8 PRIMARY KEY,
name varchar(32)
);

create table users(
id serial8 primary key,
login varchar(32),
password varchar(32),
role_id bigint references roles(id)
);

create table review_code(
id serial8 primary key,
code text,
accepted boolean,
user_id bigint references users(id),
reviewer_id bigint references users(id),
add_date timestamp,
accept_date timestamp,
update_date timestamp,
notice varchar(128)
);

create table problems(
id serial8 primary key,
problem varchar(128),
soluted boolean,
review_code_id bigint references review_code(id),
add_date timestamp,
solution_date timestamp
); 